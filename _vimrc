source ~/.vim_dotfiles/vimrc.bundle
source ~/.vim_dotfiles/vimrc.base
source ~/.vim_dotfiles/vimrc.completion
source ~/.vim_dotfiles/vimrc.statusline
source ~/.vim_dotfiles/vimrc.neocomplete
source ~/.vim_dotfiles/vimrc.vimwiki

